%Con imagen

[u,v]=meshgrid (-10:10);
g=exp(-(u.^2+v.^2)/2);
g=g/sum(g(:)); %la normalizamos
%surf(u,v,g) %campana

load durer
X=X/max(X(:));
figure, imshow (X)
figure, imshow (conv2(X,g))

gx=u.*g;
gy=v.*g;

figure, imshow (conv2(X,gx))
title('gx')
figure, imshow (conv2(X,gy))
title ('gy')

c