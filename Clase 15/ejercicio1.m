%Filtrado
N=10;%a N grande matas frecuencias mas bajas 
Fs=1000;
deltat=1/Fs;
t=(0:10000)*deltat;
w=10;
s0=sin(w*t);
ruido=0.1;
s=s0+ruido*randn(1,10001);
subplot (3,1,1)
plot(t,s0,'r',t,s,'g')
title ('sen (wt)')

%caso trivial 'media movil'
a= ones(1,1);
b= ones(1,N)*1/(N+1);
sf=filter(b,a,s);
subplot (3,1,2)
plot (t,sf,'r',t,s,'b') %el filtrado tiene menos ruido 
title ('sen(wt) con filtro')

s1= randn(1,10001);
S1=fft(s1);
n=length(S1);
freq=linspace(0,((Fs+1)/2),n);
sf1=filter(b,a,s1);
subplot (3,1,3)
plot (freq,abs(fft(s1)),'b',freq,abs(fft(sf1)),'c') %en este grafico vemos 