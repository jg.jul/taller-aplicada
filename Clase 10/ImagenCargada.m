%load mandrill; esta es una imagen simple
A=imread('./HedyLamarr.jpg'); %esta es una imagen con cada color por separado
X=double(A);
R=X(:,:,1);
G=X(:,:,2);
B=X(:,:,3);
%[U,S,V] = svd(X);
[Ur,Sr,Vr]=svd(R);
[Ug,Sg,Vg]=svd(G);
[Ub,Sb,Vb]=svd(B);
for i=1:220;
    Y(:,:,1)=Ur(:,1:i)*Sr(1:i,1:i)*(Vr(:,1:i)');
    Y(:,:,2)=Ug(:,1:i)*Sg(1:i,1:i)*(Vg(:,1:i)');
    Y(:,:,3)=Ub(:,1:i)*Sb(1:i,1:i)*(Vb(:,1:i)');
    imshow(uint8(Y))
    title(int2str(i))
    pause;
    end