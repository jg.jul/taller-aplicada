%Reactivos 
clear all
%t=tiempo, y=[concentracdión,temperatura]
tspan= [1,10];
lamda=0.5;
beta=0.3;
x0=1;
y0=0.5;

F= @ (t,y) [-y(1)*exp(-1./y(2))+lamda*(x0-y(1)); y(1)*exp(-1./y(2))+ beta*(y0-y(2))]
[t,y]= ode45 (F,tspan,[x0;y0]);
plot (t,y)

%% Puntos de equilibrio [Igualo y=0, despejo a mano y(1) en funcion de y(2)]
tspan= [1,10];
lamda=0.5;
beta=0.3;
x0=1;
y0=0.5;
y=[0.5:0.1:2];

Eq= @(y)[-beta*(y0-y)./(exp(-1./y));lamda.*x0./((exp(-1./y))+lamda)]
E= Eq(y);
hold on 
plot (E(1,:),y)
plot (E(2,:),y)
hold off

%%
lamda=0.5;
beta=0.3;
x0=1;
y0=0.5;
y=[0.5:0.1:2];

%Agrego variable t porque Ode45 necesita dos parámetros

%El punto de equilibrio es (0.519,1.3) armo malla alrededor del punto
z=linspace (0.5,0.54,100);
w=linspace(1,1.5,100);
[Z,W]=meshgrid(z,w);

for i=1:50
    for j=1:50
        Eq= @(t,y)[-beta*(y0-y(2))./(exp(-1./y(2)));lamda.*x0./((exp(-1./y(2)))+lamda)];
 [X,Y]=ode45 (Eq,[0,10],[Z(i,j);W(i,j)]);
 hold on
    end
end

plot (X,Y)

%% Malla con circunferencias 
tspan= [0,10];
x0=1;
y0=0.5;
radio = 10;
angulos = linspace(1,60,200)
lamda=0.5;
beta=0.3;

for i =1 :length(angulos)
     x1=x0 + radio*cos((2*pi*i)/max(angulos));
     y1=y0 + radio*sin((2*pi*i)/max(angulos));
     inicial= [x1,y1];
   
     F=@(x,y) [-y(1)*exp(-1./y(2))+lamda*(x0-y(1)); y(1)*exp(-1./y(2))+ beta*(y0-y(2))];
     opciones.RelTol=1e-8;
     opciones.AbsTol=1e-8;
     [x,y]= ode45(F,tspan,inicial',opciones);
     plot(y(:,1),y(:,2))
     hold on
 end

