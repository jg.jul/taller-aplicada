%Seguimos trabajando con la ecuación de los reactivos. Novedad los datos iniciales se los damos a nosotres
lamda=10;
beta=1;
x0=10;
y0=0.1;
r=1;
F= @(x,y) [-y(1).*exp(-1./y(2))+lamda.*(x0-y(1)) ; y(1).*exp(-1./y(2))+beta.*(y0-y(2))];
%F @(x,y) 
I=[0,100];

while true
    [x,y]=ginput(1);
    ini=[x,y] ; %valores iniciales, te deja volver a elegir
    [X,Y]=ode45(F,I,ini);
    hold on
    plot(Y(:,1),Y(:,2))
end

% ginput toma valores del grafico 
% Lo que estamos haciendo es similar al meshgrid pero en lugar de generar una malla con valores iniciales, le indicamos de donde queremos que arranque
% while me permite seguir corriendo el programa
