l=10;
b=0.56;
x0=10;
y0=0.1;
r=1;
F= @(x,y) [-y(1).*exp(-1./y(2))+l.*(x0-y(1)) ; y(1).*exp(-1./y(2))+l.*(y0-y(2))];
%F @(x,y) %puedo elegir la que mas me guste
I=[0,10];
while true
    [x,y]=ginput(1);
    ini=[x,y] ; %valores iniciales, te deja volver a elegir
    [X,Y]=ode45(F,I,ini);
    hold on
    plot(Y(:,1),Y(:,2))
end