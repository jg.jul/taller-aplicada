function [auto]=autoval(x0,y0)
beta=1;
lamda=10;
% x0=10;
% y0=0.1;

y=linspace(y0,x0/beta+y0,1000);
g=@(y) lamda*x0./(lamda+exp(-1./y)) - exp(1./y).*(y-y0)*beta;
xaux=g(y);
indices=find((xaux(1:999).*xaux(2:1000))<0); %indices donde cambio de signo
for i=1:length(indices)
  yeq(i)=fzero(g,y(indices(i)+[0 1]));
  xeq(i)=lamda*x0/(lamda+exp(-1/yeq(i)));
end

 for i=1:length(indices)
   A= [-exp(-1/yeq(i))-lamda, -xeq(i)*exp(-1/yeq(i))/yeq(i)^2;
       exp(-1/yeq(i)), xeq(i)*exp(-1/yeq(i))/yeq(i)^2-beta];
   auto(:,i)=eig(A); %autovalores de A
 end