function [y]= graficador(beta,lamda,x0,y0)

%beta=1;
%lamda=10;
%x0=10;
%y0=0.1;

while true
  [x1,y1] = ginput(1);
 f=@(t,y) [-y(1)*exp(-1/y(2))+lamda*(x0-y(1)) ; y(1)*exp(-1/y(2))+beta.*(y0-y(2))];
  [t,y]=ode45(f,[0 10],[x1 y1]);
  
%  hold all
%  subplot(2,1,1)
%  plot(t,y)
%  legend('concentracion','temperatura')
%  hold all
%  subplot(2,1,2)
  hold on
  plot(y(:,1),y(:,2))
end


%f1=@(x) (lamda*x0)./(lamda.+e.**(1./x));
%f2=@(y) beta.*e.**(1./y).*(y-y0);
%t=linspace(0.100000000001,10,1000);
%hold on
%semilogy(t,f1(t))
%semilogy(t,f2(t))