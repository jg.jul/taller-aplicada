beta=1;
lamda=10;
x0=10;
%y0=0.1;

f1=@(y) y-lamda*x0./(beta*(1+lamda*exp(1./y)));
g1=@(y) y-y.^2.*(1+lamda*exp(1./y))./(lamda*exp(1./y));
g2=@(y) y.^2.*beta.*(1+lamda*exp(1./y)).^2./(lamda^2*exp(1./y));
%aux=@(y) y-(lamda*x0)/(beta*(1+lamda));
y=linspace(0,10,1000);
t=ones(1000)*0.1;
y1=linspace(0,1,1000);
subplot(2,1,1)
hold on
plot(y,f1(y))
plot(y,t)
subplot(2,1,2)
plot(g1(y1),g2(y1)) %axis([0 1 0 10])
