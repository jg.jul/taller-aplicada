% Graficamos los reactivos dándole valores iniciales 

function [y]= graficador(beta,lamda,x0,y0)
F= @(x,y) [-y(1).*exp(-1./y(2))+lamda.*(x0-y(1)) ; y(1).*exp(-1./y(2))+beta.*(y0-y(2))];
I=[0,100];

while true
    [x,y]=ginput(1);
    ini=[x,y] ; %valores iniciales, te deja volver a elegir
    [X,Y]=ode45(F,I,ini);
    hold on
    plot(Y(:,1),Y(:,2))
end
