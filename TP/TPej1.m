%Ejercicio 1
[y1,Fs1]=audioread('barra5.wav');
[y2,Fs2]=audioread('barra5_torsion.wav');

Y1=fft(y1);
N1=length(y1);
if mod(N1,2)==0 
n1=N1/2+1;
else 
n1=(N1+1)/2;
end
sound (y1,Fs1)
freq1=linspace(0,(Fs1/2),n1); 
figure 
subplot (2,1,1)
plot(freq1,abs(Y1(1:n1)))
xlabel ('Frecuencia')
title ('Barra de metal')

Y2=fft(y2);
N2=length(y2);
if mod(N2,2)==0 
n2=N/2+1;
else 
n2=(N2+1)/2;
end
freq2=linspace(0,(Fs2/2),n2); 
subplot(2,1,2)
plot(freq2,abs(Y2(1:n2)))
xlabel('Frecuencia')
title ('Barra de metal con torsión')

