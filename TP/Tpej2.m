%Ejercicio 2
[y,Fs]=audioread('Bells.wav');

Y=fft(y);
N=length(y);
if mod(N,2)==0 
n=N/2+1;
else 
n=(N+1)/2;
end
sound (y,Fs)
freq=linspace(0,(Fs/2),n);
figure 
plot(freq,abs(Y(1:n)))
xlabel ('Frecuencia')
title ('Nota #La cantada')
