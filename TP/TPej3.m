%Ejercicio 3
[y,Fs]=audioread('Guitarra.wav');

Y=fft(y);
N=length(y);
if mod(N,2)==0 
n=N/2+1;
else 
n=(N+1)/2;
end
sound (y,Fs)
freq=linspace(0,(Fs/2),n); 
subplot (2,1,1) 
plot(freq,abs(Y(1:n)))
xlabel ('Frecuencia')
title ('Nota #LA en Guitarra')

[y1,Fs1]=audioread('Ukelele.wav');

Y1=fft(y1);
N1=length(y1);
if mod(N1,2)==0 
n1=N1/2+1;
else 
n1=(N1+1)/2;
end
sound (y1,Fs1)
freq1=linspace(0,(Fs1/2),n1); 
subplot (2,1,2) 
plot(freq1,abs(Y1(1:n1)))
xlabel ('Frecuencia')
title ('Nota #LA en Ukelele')