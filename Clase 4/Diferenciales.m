%%

%Ejemplo

tspan = [0 5];
y0 = 0;
[t,y] = ode45(@(t,y) 2*t, tspan, y0);


%%

%Poblacion

y0= 0:0.1:3;
lamda=3;
gama=2;
tspen= [0,5];
[x,y]= ode45(@(x,y)y.*(lamda-gama.*y),tspen, y0);

% punto de equilibrio lamda/gama 
%Si mi poblacion se pasa del punto de equilibrio analizar que pasa con la poblacion 

%%

%Cazador-Presa
tspan= [0,10];
p=1;
c=20;
a=30;
b=20;
delta= 50; 
gama=0.2;
n=0.2;

y0= [c,p];
F= @(x,y) [-a*y(1)+b*y(1)*y(2),delta *y(2)-gama*y(2)*y(2)-n*y(1)*y(2)]'
opciones.RelTol=1e-8;
opciones.AbsTol=1e-8;
[x,y]= ode45 (F,tspan,y0',opciones);
plot(x,y)
plot (y(:,1),y(:,2)) %predadores en funcion de presas
