%Aproximacion de integrales con la regla del Trapecio

function [A]=mitrapecio (f,a,b,n)
x=linspace (a,b,n);
Y=zeros(n,1);
Y=f(x);
A=trapz(x,Y); %clacula el área usando el método de trapecio

%Ahora vamos a ver cómo funciona con diversas funciones: sin(x),
%exp(x.*cos(x))
%Queremos comparar este resultado con aquel que nos devuelven los
%comandos "integral" o resolviendo a mano
%q=integral(f,a,b,opciones)
%opciones.RelTol=1e-8;
%opciones.AbsTol=1e-8;
