%Método de potencias con la inversa, para encontrar el mayor autovalor

A= [8 6; 6 8];
x(:,1)= [2;3];
u=11;
for     i=1:100
        x(:,i+1)= (A-u*(eye(2)))\x(:,i); %el mayor autovalor de A, pasa a ser el menor Autovalor de su inversa
        x(:,i+1)= x(:,i+1)/norm(x(:,i+1));
        lamda(i) = x(:,i+1)'*A*x(:,i+1);
end

lamda(100) %aproxima al autovalor más cercano a "u"
[x,S]=eig(A);
S
plot (lamda) %para ver la convergencia de lamda
semilogy(abs(14-lamda)) %error