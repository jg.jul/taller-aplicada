% Método de potencias el resultado tiende a un autovector

A= [8 6; 6 8];
x(:,1)= [2;3];
for     i=1:100
        x(:,i+1)= A\x(:,i)
        x(:,i+1)= x(:,i+1)/norm(x(:,i+1));
end
x=x(:,100) %aproximación al autovector, lo comparamos con el eigshow

[X,S]=eig (A);
X

