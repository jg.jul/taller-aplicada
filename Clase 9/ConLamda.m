%Método de potencias, con inversa, variando ahora el lamda

A= [8 6; 6 8];
x(:,1)= [10;3];
x(:,1)= (1/norm(x(:,1)))*x(:,1);
lamda(1)=x(:,1)'*A*x(:,1);

for     i=1:100
        x(:,i+1)= (A-lamda(i).*(eye(2)))\x(:,i); %resolver (A-lamda I)x=x
        x(:,i+1)= x(:,i+1)/norm(x(:,i+1));
        lamda(i+1) = x(:,i+1)'*A*x(:,i+1);
end

lamdaFinal=lamda(100)%me devuelve aproximación al mayor autovalor 
[X,S]=eig(A);
MaxAutoval=max(diag(S))
semilogy(abs(MaxAutoval-lamda)) %error
