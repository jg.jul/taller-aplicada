%Transformada de Fourier
load laughter.mat %cargamos audio de una risa y nos devuelve la info (y,Fs) sonido+frecuencia
Y=fft(y);%nos devuelve un vector de la transformada de fourier 
N=length(y);
freq=linspace(0,(Fs/2),(N/2)+1); 
plot(freq,abs(Y(1:N/2+1)))
%plot(abs(Y)) %simétrico pues y es real en este caso en particular

%% manchas solares 

load sunspot.dat
anio=sunspot(:,1);
cantmanchas =sunspot(:,2);
%plot(anio,cantmanchas)
N=length(cantmanchas);
Y=fft(cantmanchas); %aproximamos la cantidad de manchas con fourier
freq=linspace (0,1/2,N/2+1);
plot (freq,abs(Y(1:N/2+1)))
%consideramos el punto mas alto del grafico pues nos marcara la
%periodicidad 

%% tono de telefono

fr =[697 770 852 941];
fc=[1209 1336 1477];
Fs=10000;
t=(1:10000)/Fs;
N=length (t);
cinco=1/2*(sin(2*pi*770*t)+sin(2*pi*1336*t));
sound (cinco,Fs)
freq=linspace (0,Fs/2,N/2+1);
Y=fft(cinco);%me da las frecuencias con peso que necesito para construir "cinco"
plot(freq,abs(Y(1:N/2+1)))
%plot (cinco)