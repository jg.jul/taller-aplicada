%Autovectores 
hold off
p=poly([1,2,3]); %me devuelve un polinomio con estos coeficientes
x=linspace (min(p)-1,max(p)+1,1000);
plot (x,polyval(p,x),roots(p),[0,0,0],'r')
