v=@(t) sin(2*pi*1000*t); %señal de frencencia de 1000Hz

fs1=20000;
t=linspace(0,0.01,0.01*fs1);
subplot (4,1,1)
plot(t,v(t),'b')
title ('fs/2>10³')

fs2=2500;
t2=linspace(0,0.01,0.01*fs2);
subplot (4,1,2)
plot(t2,v(t2),'g')
title('fs')

fs3=700;
t3=linspace(0,0.01,0.01*fs3);
subplot (4,1,3)
plot(t3,v(t3),'r')
title ('fs/2<10³')

subplot (4,1,4)
plot(t3,v(t3),'r',t2,v(t2),'g',t,v(t),'b')
