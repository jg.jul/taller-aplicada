%Pendulo con sistema dinamico discreto con runge kutta 
clear u1 u2
deltat=0.1; %fijo
alfa=2; %-(gravedad/longitud de la cuerda)
u1(1)=1; %angulo
u2(1)=0; %velocidad

for i=1:50000
    u1(i+1)=u1(i)+deltat/2*(u2(i)+u2(i)+deltat*(-alfa*sin(u1(i))));
    u2(i+1)=u2(i)+deltat/2*((-alfa*sin(u1(i)))-alfa*sin(u1(i)+deltat*u2(i)));
end

plot(u1,u2) %deberia dar un circulo
