%Clase 11, seguimos con método SVD 

A1=[2 3;5 1]; %analizamosmétodo con m=n
[U1,S1,V1]=svd(A1)
b1= [4;5];
x1=A1\b1
y1= V1*inv(S1)*U1*b1 %comparo el resultado hallado con \ y con svd
%%
A2=[2 3;5 6; 8 -1]; %m>n
[U2,S2,V2]=svd(A2)
b2=[ 1; 2; 3];
x2=A2\b2
y2= V2*inv(S2)*U2*b2
%%
A3= [ 2 6 3; 5 4 9]; %m<n
[U3,S3,V3]=svd(A3);
b3=[1; 5];
x3=A3\b3
InvS3=[1/S3(1,1) 0 ;0 1/S3(2,2); 0 0];
y3= V3*InvS3*U3*b3
%%
A4=[2 6 3; 5 4 9]; %m<n
[U4,S4,V4]=svd(A4);
b4=[1; 5];
x4=A4\b4 %tiene error maoyor, capaz no está en la imagen de A4
S5=S4(:,1:2);%tomo todas las columnas desde la 1 hasta la 2 "1:2" son los valores más importantes de la matriz
y4=V4(:,1:2)*inv(S5)*U4'*b4 %este está en la imagen de A4 porque está generado con las bases
z4=pinv(A4)*b4 %pseudo inversa 
Normax4=norm(x4)
Normay4=norm(y4)