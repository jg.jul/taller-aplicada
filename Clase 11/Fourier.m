%Ejemplo con Fourier, temperaturas


J=[1 2 6 4 5 1];
A=toeplitz(J); %genera matriz circulante
x=[2 5 1 6 5 3];
b=A*x'
bruido=b+randn(size(b)) %"b+ruido"
xruido=A\bruido %resolvemos el sistema para ver con cuánto error nos devulve x
plot ([xruido x'],'-o') %me devuelve donde está cada coordenada 

%% 
n=50;
J=linspace(1,20,n); %vector que crece todo el tiempo hasta que cae, no tengo una gaussiana por eso el error es tan grande 
A=toeplitz(J); 
x=rand([1 n]);
b=A*x';b=A*x'
bruido=b+randn(size(b));
xruido=A\bruido ;
plot ([xruido x'],'-o')